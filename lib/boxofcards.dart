
import 'package:test_app/card.dart';
import 'package:flutter/material.dart';


const String _c = "club";
const String _d = "diamond";
const String _h = "heart";
const String _s = "spade";

class CardPair {
  final String val;
  final String suit;
  const CardPair(this.val, this.suit);
}

class BoxOfCards {
  static const _cards = <CardPair>[
    CardPair("A", _c),
    CardPair("2", _c),
    CardPair("3", _c),
    CardPair("4", _c),
    CardPair("5", _c),
    CardPair("6", _c),
    CardPair("7", _c),
    CardPair("8", _c),
    CardPair("9", _c),
    CardPair("10", _c),
    CardPair("J", _c),
    CardPair("Q", _c),
    CardPair("K", _c),
    CardPair("A", _d),
    CardPair("2", _d),
    CardPair("3", _d),
    CardPair("4", _d),
    CardPair("5", _d),
    CardPair("6", _d),
    CardPair("7", _d),
    CardPair("8", _d),
    CardPair("9", _d),
    CardPair("10", _d),
    CardPair("J", _d),
    CardPair("Q", _d),
    CardPair("K", _d),
    CardPair("A", _h),
    CardPair("2", _h),
    CardPair("3", _h),
    CardPair("4", _h),
    CardPair("5", _h),
    CardPair("6", _h),
    CardPair("7", _h),
    CardPair("8", _h),
    CardPair("9", _h),
    CardPair("10", _h),
    CardPair("J", _h),
    CardPair("Q", _h),
    CardPair("K", _h),
    CardPair("A", _s),
    CardPair("2", _s),
    CardPair("3", _s),
    CardPair("4", _s),
    CardPair("5", _s),
    CardPair("6", _s),
    CardPair("7", _s),
    CardPair("8", _s),
    CardPair("9", _s),
    CardPair("10", _s),
    CardPair("J", _s),
    CardPair("Q", _s),
    CardPair("K", _s),
  ];
  
  static DCard getAt(int index)
  {
      return DCard(_cards[index].val, _cards[index].suit);
  }

  static List<Widget> cardsFromList(List<int> l) {
    double start = 50;
    List<Widget> wl = List<Widget>.generate(l.length, (int index) {
      return Positioned(
        top: (index * 100) + start,
        child: getAt(l[index]),
      );
    });
    return wl;
  }

  static Widget createScrollableStack(List<int> l) {
    return Container(
      alignment: Alignment.centerRight,
      child: ListView(
        //padding: EdgeInsets.all(50.0),
        //shrinkWrap: true,
        //controller: sc,
        children: [
          Container(
            height: l.length * 100.0 + 600,
            child: Stack(
              alignment: Alignment.center,
              children: cardsFromList(l),
            ),
          ),
        ],
      ),
    );
  }
}
