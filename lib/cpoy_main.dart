// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// You can read about packages here: https://flutter.io/using-packages/
import 'package:flutter/material.dart';
import 'dart:math';

// You can use a relative import, i.e. `import 'category.dart;'` or
// a package import, as shown below.
// More details at http://dart-lang.github.io/linter/lints/avoid_relative_lib_imports.html
import 'package:test_app/category.dart';
import 'package:test_app/card.dart';
import 'package:test_app/deck.dart';
import 'package:test_app/screen.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:test_app/cardorder.dart';
import 'package:test_app/cardcount.dart';

const _categoryName = 'Cake';
const _categoryIcon = Icons.favorite;
const _categoryColor = Colors.green;
String tval = "1";

/// The function that is called when main.dart is run.
void main() {
  runApp(CardApp("2"));
}

/// This widget is the root of our application.
/// Currently, we just show one widget in our app.
class CardApp extends StatelessWidget {
  final String _name;
  final ScrollController intScrollController = ScrollController();
  CardApp(this._name);
  @override
  Widget build(BuildContext context) {
    print(_name);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cards',
      home: Scaffold(
        backgroundColor: Colors.grey[850],
        body: Center(
          child: Column(
            children: [
              Container(
                height: 50,
              ),
              Deck(),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CardOrder(
                      onChanged: (val){
                        print("current state $val");
                      },
                    ),
                    CardCount(
                      onChanged: (val) {
                          print("count $val");
                      },
                      count: 45,
                    ),
                    /*
                    FloatingActionButton.extended(
                      onPressed: () {
                        print(intScrollController.offset);
                      },
                      isExtended: true,
                      icon: Icon(Icons.view_agenda),
                      label: Text("val"),
                      heroTag: "btnv",
                      //shape: CircleBorder() ,
                    ),
                    */
                    /*
                    SelectionButton(
                      onChanged: (num){
                        print("Main class $num");
                      },
                    ),
                    */
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
