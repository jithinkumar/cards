import 'package:test_app/shuffle.dart';

class GuessCard {
  Dealer dlr;
  final int count;
  int _iteration;
  int _middlecount;
  int _found;
  List<int> deck;
  GuessCard({this.count=21}) {
    _iteration=0;
    _middlecount=0;
    dlr = Dealer(
      count:this.count,
      shuffle: true,
      );
      deck = dlr.giveAll();
  }
  List<int> getAll() {
    return deck;
  }
  int getFound() {
    return _found;
  }
  //pos should be greater than 0
  //for deck1, deck2, deck3
  List<int> getDeck(int pos){
    print("Deck $pos");
    List<int> l = [];
    for (var i = (pos-1); i < count; i= i+3) {
      l.add(deck[i]);
      print(deck[i]);
    }
    return l;
  }
  bool nextIter(int chosen)
  {
    _iteration++;
 
    if(_iteration == 3)
    {
      print("found");
      List<int> ch = getDeck(chosen);
      print(ch[ch.length ~/ 2]);
      _found = ch[ch.length ~/ 2];
      return true;
    }

    List<int> l = List.generate(3,(int index) => index+1);
    l.remove(chosen);

    List<int> ch = getDeck(chosen);
    List<int> l1 = getDeck(l[0]);
    l1.shuffle();
    List<int> l3 = getDeck(l[1]);
    l3.shuffle();
    deck.clear();
    deck.addAll(l1.getRange(0, l1.length));
    deck.addAll(ch.getRange(0, ch.length));
    deck.addAll(l3.getRange(0, l3.length));
    return false;
   }

   int getIteration()
   {
     return _iteration;
   }
 
}