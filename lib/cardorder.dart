import 'package:flutter/material.dart';

class CardOrder extends StatefulWidget {
  final ValueChanged<bool> onChanged;
  bool shuffled;
  CardOrder({
    @required this.onChanged,
    this.shuffled = false,

  });
  @override
  _CardOrderState createState() => _CardOrderState();
}

class _CardOrderState extends State<CardOrder> {
  String _s;
  Icon _i;
  bool _shuffled;
  @override
  void initState() {
    _shuffled = widget.shuffled;
    _s = "Sorted";
    _i = Icon(Icons.sort);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      onPressed: () {
        _shuffled = !_shuffled;
        if (_shuffled) {
          _s = "Shuffled";
          _i = Icon(Icons.shuffle);
        } else {
          _s = "Sorted";
          _i = Icon(Icons.sort);
        }
        widget.onChanged(_shuffled);
        setState(() {});
      },
      //isExtended: true,
      icon: _i,
      label: Text(_s),
      heroTag: "btns",
      //shape: CircleBorder() ,
    );
  }
}
