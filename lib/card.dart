import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'dart:math';
import 'package:test_app/shuffle.dart';

class Invert extends StatelessWidget {
  final StatelessWidget w;
  Invert(this.w);
  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.rotationZ(pi),
      alignment: Alignment.center,
      child: w,
    );
  }
}

class CardText extends StatelessWidget {
  final String val_;
  final double size_;
  final double width_;
  final Color color_;
  CardText({
    Key key,
    @required this.val_,
    @required this.size_,
    @required this.color_,
    this.width_ = 50.0,
  })  : assert(val_ != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width_,
      //height: 300,
      //alignment: Alignment.bottomRight,
      child: Text(
        val_,
        style: TextStyle(
          fontSize: size_,
          color: color_,
        ),
        textAlign: TextAlign.left,
      ),
    );
  }
}

class CardIcon extends StatelessWidget {
  final double width_;
  final double scale_;
  final String asset_;
  final AlignmentGeometry alignment_;
  CardIcon({
    Key key,
    @required this.asset_,
    @required this.width_,
    this.alignment_ = Alignment.center,
    this.scale_ = 2.5,
  })  : assert(asset_ != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width_,
      alignment: alignment_,
      child: Image.asset(
        asset_,
        scale: scale_,
      ),
    );
  }
}

class SuitIcon extends StatelessWidget {
  final double width;
  final double scale;
  final AlignmentGeometry alignment;
  final String suit;
  SuitIcon(this.suit, this.width, this.scale, this.alignment);
  @override
  Widget build(BuildContext context) {
    return CardIcon(
      width_: width,
      alignment_: alignment,
      asset_: get_suit(suit),
      scale_: scale,
    );
  }

  String get_suit(String suit) {
    switch (suit) {
      case "club":
        return 'images/club.png';
      case "heart":
        return 'images/heart.png';
      case "diamond":
        return 'images/diamond.png';
      case "spade":
        return 'images/spade.png';
      default:
        return 'images/spade.png';
    }
  }
}

class TextIcon extends StatelessWidget {
  final String suit;
  final String val;
  final double width;
  TextIcon(this.suit, this.val, {this.width = 50.0});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      CardText(
        val_: val,
        size_: 44,
        color_: get_color(suit),
        width_: width,
      ),
      SuitIcon(suit, width, 2.7, Alignment.centerLeft),
    ]);
  }

  Color get_color(String suit) {
    if (suit == "spade" || suit == "club") {
      return Colors.black;
    } else if (suit == "heart" || suit == "diamond") {
      return Colors.red;
    } else {
      return Colors.black;
    }
  }
}

class DCard extends StatelessWidget {
  final String suit;
  final String val;
  DCard(this.val, this.suit);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      width: 300,
      padding: const EdgeInsets.all(8.0),
      //color: Colors.white,
      decoration: BoxDecoration(
        color: Colors.grey[200],
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
            color: Colors.blue,
            blurRadius: 10.0,
          ),
        ],
      ),
      child: Row(
        children: [
          TextIcon(suit, val),
          SuitIcon(suit, 180, 1.0, Alignment.center),
          Invert(TextIcon(suit, val)),
        ],
      ),
    );
  }
}
