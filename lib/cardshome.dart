import 'package:flutter/material.dart';
import 'package:test_app/test.dart';
import 'package:test_app/cardpicker.dart';

class CardsHome extends StatelessWidget {
  Decoration getDecoration({Color c = Colors.grey}) {
    return BoxDecoration(
      color: c,
      shape: BoxShape.rectangle,
      borderRadius: BorderRadius.circular(7.0),
      boxShadow: [
        BoxShadow(
          color: Colors.blue,
          blurRadius: 20.0,
        ),
      ],
    );
  }

  Widget getCard(String val, Color c, IconData icon) {
    return Container(
      width: 250,
      height: 200,
      padding: EdgeInsets.all(16.0),
      alignment: Alignment.center,
      decoration: getDecoration(c: c),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(icon,
            size: 70,
          ),
          Text(
            val,
            style: TextStyle(
              fontSize: 52,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //width: 300,
      //height: 600,
      /*
      decoration: BoxDecoration(
        color: Colors.grey[850],
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.blue,
            blurRadius: 10.0,
          ),
        ],
      ),
      */
      appBar: AppBar(
        title: Text("HOME OF CARDS",
        ),
        backgroundColor: Colors.grey[850],
        centerTitle: true,
      ),
      //alignment: Alignment.center,
      backgroundColor: Colors.grey[850],
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          InkWell(
            onTap: () {
              print("Just Cards");
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CardPicker(),
                ),
              );
            },
            child: getCard("Just Cards", Colors.green,Icons.credit_card),
          ),
          InkWell(
            onTap: () {
              print("Card Trick");
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InitGuessGame(),
                ),
              );
            },
            child: getCard("Card Trick", Colors.orange,Icons.local_play),
          ),
        ],
      ),
    );
  }
}
