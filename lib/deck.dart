import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:test_app/shuffle.dart';
import 'package:test_app/boxofcards.dart';

class Deck extends StatefulWidget {
  final int count;
  final bool shuffle;
  const Deck({Key key, this.count = 52, this.shuffle = false})
      : super(key: key);
  @override
  _DeckState createState() => _DeckState();
}

class _DeckState extends State<Deck> {
  //int _i = 0;
  Dealer dealer;
  PageController p;
  int _page;
  bool _page_change;

  @override
  void initState() {
    //print("Deck Made Init");
    dealer = Dealer(
      count: widget.count,
      shuffle: widget.shuffle,
    );
    //_val = _cards[dealer.give()].val;
    //_suit = _cards[dealer.give()].suit;
    p = PageController(
      viewportFraction: 2.0,
    );
    super.initState();
    _page = 1;
    _page_change = false;
  }

  List<Widget> _getCards() {
    List<int> l = dealer.giveAll();
    List<Widget> wl = List<Widget>.generate(l.length, (int index) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BoxOfCards.getAt(l[index]),
        ],
      );
    });
    return wl;
  }

  @override
  Widget build(BuildContext context) {
    if (!_page_change) {
      dealer = Dealer(
        count: widget.count,
        shuffle: widget.shuffle,
      );
    }
    else {
      _page_change = false;
    }
    return Container(
      height: 500,
      //width: 300,
      //padding: EdgeInsets.all(50.0),
      //padding: EdgeInsets.only(left: 20,right: 20),
      alignment: Alignment.center,
      //width: 1000,

      child: Stack(
        children: <Widget>[
          PageView(
            controller: p,
            children: _getCards(),
            onPageChanged: (page) {
              setState(() {
                _page_change = true;
                _page = page + 1;
              });
            },
          ),
          Positioned(
            top: 20,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.credit_card),
              label: Text(
                "$_page",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
              backgroundColor: Colors.green,
              foregroundColor: Colors.black,
              heroTag: "ggbb",
              onPressed: () {
                print("pressed");
              },
            ),
          ),
        ],
      ),
    );
  }
}
