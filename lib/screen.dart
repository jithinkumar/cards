import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class SelectionButton extends StatelessWidget {
  final ValueChanged<num> onChanged;
  SelectionButton({
    @required this.onChanged,
  });
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      onPressed: () {
        _navigateAndDisplaySelection(context);
      },
      //isExtended: true,
      heroTag: "btc",
      icon: Icon(Icons.input),
      label: Text("Count"),
      //shape: CircleBorder() ,
    );
  }

  // A method that launches the SelectionScreen and awaits the result from
  // Navigator.pop!
  _navigateAndDisplaySelection(BuildContext context) async {
    // Navigator.push returns a Future that will complete after we call
    // Navigator.pop on the Selection Screen!
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SelectionScreen()),
    );
    print(result);
    onChanged(result);
    return result;
  }
}

class SelectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            height: 200,
            //width: 200,
            alignment: Alignment.center,
            child: MyCustomForm()),
      ),
    );
  }
}

// Define a Custom Form Widget
class MyCustomForm extends StatefulWidget {
  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

// Define a corresponding State class. This class will hold the data related to
// our Form.
class _MyCustomFormState extends State<MyCustomForm> {
  // Create a text controller. We will use it to retrieve the current value
  // of the TextField!
  final myController = TextEditingController();
  int val = 52;

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*
      appBar: AppBar(
        title: Text('Retrieve Text Input'),
      ),
      */
      body: Container(
        padding: const EdgeInsets.all(8.0),
        height: 300,
        alignment: Alignment.center,

        child: Column(
          children: [
            NumberPicker.integer(
              minValue: 5,
              maxValue: 52,
              initialValue: 52,
              itemExtent: 50,
              onChanged: (num) {
                print(num);
                val = num;
              },
            ),
            FloatingActionButton(
              // When the user presses the button, show an alert dialog with the
              // text the user has typed into our text field.
              onPressed: () {
                Navigator.pop(context, val);
              },
              tooltip: 'Show me the value!',
              child: Icon(Icons.verified_user),
            ),
          ],
        ),
      ),
      /*
        child: ListView(
          children: <Widget>[
            Text("1"),
            Text("2"),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        // When the user presses the button, show an alert dialog with the
        // text the user has typed into our text field.
        onPressed: () {
          Navigator.pop(context, val);
        },
        tooltip: 'Show me the value!',
        child: Icon(Icons.text_fields),
      ),
      */
    );
  }
}
