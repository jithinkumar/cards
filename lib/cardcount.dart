import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class CardCount extends StatefulWidget {
  final ValueChanged<num> onChanged;
  final count;
  CardCount({
    @required this.onChanged,
    this.count,
  });
  @override
  _CardCountState createState() => _CardCountState();
}

class _CardCountState extends State<CardCount> {
  String _s;
  Icon _i;
  bool _shuffled = false;
  int _count = 52;
  @override
  void initState() {
    _i = Icon(Icons.confirmation_number);
    super.initState();
    _count = widget.count;
    _s = _count.toString();
  }

  void _showDialog() {
    showDialog<int>(
    context: context,
      builder: (BuildContext context) {
        return new NumberPickerDialog.integer(
          minValue: 5,
          maxValue: 52,
          title: new Text("Count",
            textAlign: TextAlign.center,
          ),
          initialIntegerValue: _count,
        );
      }
    ).then((int value) {
      if (value != null) {

        setState((){ 
          _count = value;
          _s = _count.toString();
          widget.onChanged(_count); 
          });
      }
    });
    
  }


  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      onPressed: _showDialog,
      //isExtended: true,
      icon: Icon(Icons.confirmation_number),
      label: Text(_s),
      heroTag: "btnc",
      //shape: CircleBorder() ,
    );
  }
}
