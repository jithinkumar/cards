
/// This widget is the root of our application.
/// Currently, we just show one widget in our app.
/// 
/// 
import 'package:flutter/material.dart';
import 'package:test_app/deck.dart';
import 'package:test_app/cardorder.dart';
import 'package:test_app/cardcount.dart';

class CardPicker extends StatefulWidget {
  CardPicker();
  @override
  _CardPickerState createState() => _CardPickerState();
}

class _CardPickerState extends State<CardPicker> {
  bool _shuffle;
  int _count;
  @override
  @override
  void initState() {
    super.initState();
    _shuffle = false;
    _count = 52;
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cards',
      home: Scaffold(
        backgroundColor: Colors.grey[850],
        body: Center(
          child: Column(
            children: [
              Container(
                height: 50,
              ),
              SizedBox(
                //width: 225,
                //height: 375,
                child:Deck(
                  count: _count,
                  shuffle: _shuffle,
                ),
                //child: Carroussel(),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CardOrder(
                      onChanged: (val){
                        print("current state $val");                        
                        setState(() {
                          _shuffle = val;
                        });
                      },
                      shuffled: _shuffle,
                    ),
                    CardCount(
                      onChanged: (val) {
                          print("count $val");
                          setState(() {
                          _count = val;
                        });
                      },
                      count: _count,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}

