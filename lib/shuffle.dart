import 'dart:math';

class Dealer {
  int count = 52;
  int _index=0;
  /*
  var _indexs = <int>[
        0, 1, 2, 3, 4, 5, 6, 7, 8, 
       9, 10, 11, 12, 13, 14, 15, 
       16, 17, 18, 19, 20, 21, 22,  
       23, 24, 25, 26, 27, 28, 29, 
       30, 31, 32, 33, 34, 35, 36, 
       37, 38, 39, 40, 41, 42, 43,  
       44, 45, 46, 47, 48, 49, 50, 
       51];
  */
  var _indexs = List<int>.generate(52, (int index){
    return index;
  });

  void _shuffle() {
    for (var i = 0; i < 52; i++) {
        int r = i + (Random().nextInt(1000) % 52-i);
        _swap(i, r);
    }
  } 
  void _swap(int x, int y) {
  var tmp = _indexs[x];
  _indexs[x] = _indexs[y];
  _indexs[y] = tmp;
  } 
  Dealer({this.count=52,bool shuffle=false,}){
    //print("Dealer made");
    if (shuffle) {
      //_shuffle();
      _indexs.shuffle();

      /*
      for (var i = 0; i < 52; i++) {
        print(_indexs[i]);
      }
      */
    }
  }
  void next() {
    _index++;
    //print(_index);
    if(_index >= count)
    {
      _index = 0;
    }
    //return _indexs[_index];
  }

  void prev() {
    _index--;
    //print(_index);
    if(_index < 0)
    {
      _index = count-1;
    }
    //return _indexs[_index];
  }
  int give() {
    if(_index < 0){
      _index = count - 1;
    }
    else if(_index >= count){
      _index = 0;
    }
    return _indexs[_index];
  }
  int at(int index) {
    return _indexs[index];
  }  
  List<int> giveAll()
  {
    return _indexs.sublist(0,count);
  }
}