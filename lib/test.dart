import 'package:flutter/material.dart';
import 'package:test_app/guesscard.dart';
import 'package:test_app/boxofcards.dart';
import 'dart:async';

enum Answers { YES, NO }
const String instructions = "1.Select the deck in which your card present\n2.Swipe right of left to switch between decks\n3.Scroll up or down to see all cards in a deck";

class GuessGame extends StatefulWidget {
  final GuessCard gc;
  GuessGame(this.gc);
  @override
  _GuessGameState createState() => _GuessGameState();
}

class _GuessGameState extends State<GuessGame> {
  PageController pc = PageController();
  int _deck;
  Color _c;
  //ScrollController sc = ScrollController();
  void _nextRound(BuildContext c) {
    bool found = widget.gc.nextIter(_deck);
    if (!found) {
      String val = "";
      if (widget.gc.getIteration() == 1) {
        val = "One more Time!";
      } else if (widget.gc.getIteration() == 2) {
        val = "Last Round !";
      }
      _nextPopUp(c, val);
      setState(() {
        pc.animateToPage(
          0,
          duration: Duration(
            microseconds: 250,
          ),
          curve: ElasticInCurve(),
        );
      });
    } else {
      int card = widget.gc.getFound();
      print(card);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShowCard(card),
        ),
      );
    }
  }

  Future _nextPopUp(BuildContext c, String val) async {
    bool next = await showDialog<bool>(
        context: c,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(
              val,
              textAlign: TextAlign.center,
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: const Text(
                  'OK',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
              ),
            ],
          );
        });
  }

  Future _askUser(BuildContext c) async {
    switch (await showDialog<Answers>(
        context: c,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text(
              'Are you sure ?',
              textAlign: TextAlign.center,
            ),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Answers.YES);
                },
                child: const Text(
                  'Yes!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Answers.NO);
                },
                child: const Text(
                  'No,Select again!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.blue,
                  ),
                ),
              ),
            ],
          );
        })) {
      case Answers.YES:
        {
          print("Yes");
          _nextRound(c);
          break;
        }
      case Answers.NO:
        {
          print("No");
          break;
        }
    }
  }

  //GuessCard gc = GuessCard();
  @override
  void initState() {
    _deck = 1;
    _c = Colors.orange;
    super.initState();
  }

  Color getColor(int deck)
  {
    if(deck == 1)
      return Colors.orange;
    else if(deck == 2)
      return Colors.red;
    else if(deck == 3)
      return Colors.green;
    return Colors.orange;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cards',
      //locale: const Locale('ja'),
      home: Scaffold(
        backgroundColor: Colors.grey[850],
        body: Stack(children: [
          PageView(
            controller: pc,
            onPageChanged: (page) {
              print("page $page");
              setState(() {
                _deck = page + 1;
                _c = getColor(_deck);
              });
            },
            children: [
              BoxOfCards.createScrollableStack(widget.gc.getDeck(1)),
              BoxOfCards.createScrollableStack(widget.gc.getDeck(2)),
              BoxOfCards.createScrollableStack(widget.gc.getDeck(3)),
            ],
          ),
          Positioned(
            bottom: 50,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.casino),
              label: Text("Deck $_deck",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                  ),
              ),
              backgroundColor: _c,
              foregroundColor: Colors.black,
              heroTag: "ggb",
              onPressed: () {
                print("pressed");
                print("${pc.page}");
                _askUser(context);
              },
            ),
          ),
          Positioned(
            top: 50,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.help),
              label: Text("Help  "),
              heroTag: "ggbb",
              onPressed: () {
                print("pressed");
                print("${pc.page}");
                _nextPopUp(context,instructions);
              },
            ),
          ),
        ]),
      ),
    );
  }
}

class InitGuessGame extends StatelessWidget {
  final GuessCard _gc = GuessCard();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[850],
      body: Stack(
        children: <Widget>[
          PageView(
            children: <Widget>[
              BoxOfCards.createScrollableStack(_gc.getAll()),
            ],
          ),
          Positioned(
            bottom: 50,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.casino),
              label: Text("Let's Start!"),
              heroTag: "ggaa",
              onPressed: () {
                print("pressed");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => GuessGame(_gc),
                  ),
                );
              },
            ),
          ),
          Positioned(
            top: 20,
            right: 20,
            left: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.memory),
              heroTag: "ggab",
              label: Text("Remeber A Card From Deck"),
              onPressed: () {
                print("pressed");
              },
            ),
          ),
        ],
      ),
    );
  }
}

class ShowCard extends StatelessWidget {
  final int card;
  ShowCard(this.card);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[850],
      body: Stack(
        children: <Widget>[
          PageView(
            children: <Widget>[
              BoxOfCards.createScrollableStack([card]),
            ],
          ),
          Positioned(
            bottom: 50,
            right: 50,
            left: 50,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.replay),
              label: Text("Play Again!"),
              heroTag: "ggaaa",
              onPressed: () {
                print("pressed");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => InitGuessGame(),
                  ),
                );
              },
            ),
          ),
          Positioned(
            top: 20,
            right: 20,
            left: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.card_giftcard),
              heroTag: "ggabc",
              label: Text("Your Card !"),
              onPressed: () {
                print("pressed");
              },
            ),
          ),
        ],
      ),
    );
  }
}
